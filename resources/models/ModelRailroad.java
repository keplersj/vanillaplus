package net.minecraft.src;

public class ModelRailroad extends ModelBase
{
  //fields
    ModelRenderer RightRail;
    ModelRenderer LeftRail;
    ModelRenderer Railbed1;
    ModelRenderer Railbed2;
    ModelRenderer Railbed3;
    ModelRenderer Railbed4;
  
  public ModelRailroad()
  {
    textureWidth = 68;
    textureHeight = 36;
    
      RightRail = new ModelRenderer(this, 0, 0);
      RightRail.addBox(0F, 0F, 0F, 2, 2, 16);
      RightRail.setRotationPoint(-6F, 22F, -8F);
      RightRail.setTextureSize(68, 36);
      RightRail.mirror = true;
      setRotation(RightRail, 0F, 0F, 0F);
      LeftRail = new ModelRenderer(this, 0, 18);
      LeftRail.addBox(0F, 0F, 0F, 2, 2, 16);
      LeftRail.setRotationPoint(4F, 22F, -8F);
      LeftRail.setTextureSize(68, 36);
      LeftRail.mirror = true;
      setRotation(LeftRail, 0F, 0F, 0F);
      Railbed1 = new ModelRenderer(this, 36, 0);
      Railbed1.addBox(0F, 0F, 0F, 14, 1, 2);
      Railbed1.setRotationPoint(-7F, 23F, -7F);
      Railbed1.setTextureSize(68, 36);
      Railbed1.mirror = true;
      setRotation(Railbed1, 0F, 0F, 0F);
      Railbed2 = new ModelRenderer(this, 36, 3);
      Railbed2.addBox(0F, 0F, 0F, 14, 1, 2);
      Railbed2.setRotationPoint(-7F, 23F, -3F);
      Railbed2.setTextureSize(68, 36);
      Railbed2.mirror = true;
      setRotation(Railbed2, 0F, 0F, 0F);
      Railbed3 = new ModelRenderer(this, 36, 6);
      Railbed3.addBox(0F, 0F, 0F, 14, 1, 2);
      Railbed3.setRotationPoint(-7F, 23F, 1F);
      Railbed3.setTextureSize(68, 36);
      Railbed3.mirror = true;
      setRotation(Railbed3, 0F, 0F, 0F);
      Railbed4 = new ModelRenderer(this, 36, 9);
      Railbed4.addBox(0F, 0F, 0F, 14, 1, 2);
      Railbed4.setRotationPoint(-7F, 23F, 5F);
      Railbed4.setTextureSize(68, 36);
      Railbed4.mirror = true;
      setRotation(Railbed4, 0F, 0F, 0F);
  }
  
  public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
  {
    super.render(entity, f, f1, f2, f3, f4, f5);
    setRotationAngles(f, f1, f2, f3, f4, f5);
    RightRail.render(f5);
    LeftRail.render(f5);
    Railbed1.render(f5);
    Railbed2.render(f5);
    Railbed3.render(f5);
    Railbed4.render(f5);
  }
  
  private void setRotation(ModelRenderer model, float x, float y, float z)
  {
    model.rotateAngleX = x;
    model.rotateAngleY = y;
    model.rotateAngleZ = z;
  }
  
  public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5)
  {
    super.setRotationAngles(f, f1, f2, f3, f4, f5);
  }

}
